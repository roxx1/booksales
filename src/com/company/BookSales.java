package com.company;

import com.company.models.Book;
import com.company.models.Sale;
import com.company.models.SalePaymentEnum;

import java.io.BufferedReader;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;

public class BookSales {

    public static void main(String[] args) {
        //  src/com/company/resources/books-list.csv src/com/company/resources/sales-list.csv 2 2 2018-08-01

        // write your code here
        String line1 = null, line2 = null, splitBy = ",";
        final List<Book> bookList = new ArrayList<>();
        final List<Sale> saleList = new ArrayList<>();

        try (BufferedReader br1 = new BufferedReader(new FileReader(args[0])); BufferedReader br2 = new BufferedReader(new FileReader(args[1]))) {
            br1.readLine();
            br2.readLine();
            while ((line1 = br1.readLine()) != null) {
                final String[] bookString = line1.split(splitBy);
                final Book book = new Book();
                book.setBook_id(bookString[0]);
                book.setBook_title(bookString[1]);
                book.setBook_author(bookString[2]);
                book.setBook_price(Double.parseDouble(bookString[3]));
                bookList.add(book);
            }

            while ((line2 = br2.readLine()) != null) {
                final String[] saleString = line2.split(splitBy);
                final Sale sale = new Sale();
                sale.setSale_date(new SimpleDateFormat("yyyy-dd-MM").parse(saleString[0]));
                sale.setSale_email(saleString[1]);
                sale.setSale_payment_method(SalePaymentEnum.valueOf(saleString[2]));
                sale.setSale_item_count(Integer.parseInt(saleString[3]));
                sale.setOnwards(saleString[4]);
                saleList.add(sale);
            }

            final int length = args.length;
            if (length > 2) {
                final int top_selling_books_count = Integer.parseInt(args[2]);
                final Map<String, Integer> bookMap = new HashMap<>();
                for (final Sale sale : saleList) {
                    final int count = sale.getSale_item_count();
                    final String[] bookIdWithCount = sale.getOnwards().split("&");
                    for (int i = 0; i < count; i++) {
                        final String[] idWithCount = bookIdWithCount[i].split(";");
                        // Map<Bookid,count>
                        if (bookMap.containsKey(idWithCount[0])) {
                            bookMap.put(idWithCount[0], bookMap.get(idWithCount[0]) + Integer.valueOf(idWithCount[1]));
                        } else {
                            bookMap.put(idWithCount[0], Integer.valueOf(idWithCount[1]));
                        }
                    }
                }
                // best selling book on sale value
                System.out.println("Book_id and Sale_Value ");
                bookMap.entrySet().stream()
                        .sorted(Comparator.comparing(x -> x.getValue())).limit(top_selling_books_count)
                        .forEach(book -> System.out.println(book.getKey() + "  " + book.getValue()));

                System.out.println("---------------------------------------");

                if (length > 3) {
                    final int top_customers_count = Integer.parseInt(args[3]);
                    final Map<String, Double> customerPriceMap = new HashMap<>();
                    final Map<String, List<Sale>> customerMap = saleList.stream().collect(groupingBy(Sale::getSale_email));
                    customerMap.entrySet().stream().forEach(customerEmailEntry -> {
                        final List<Sale> salesListByCustomer = customerEmailEntry.getValue();
                        double pricePerCustomer = 0.0;
                        for (final Sale sale : salesListByCustomer) {
                            pricePerCustomer += getPrice(bookList, sale.getOnwards());
                        }
                        customerPriceMap.put(customerEmailEntry.getKey(), pricePerCustomer);
                    });

                    System.out.println("Customer with PurchasedValue");
                    customerPriceMap.entrySet().stream()
                            .sorted(Comparator.comparing(entry -> entry.getValue()))
                            .limit(top_customers_count)
                            .forEach(ent -> System.out.println(ent.getKey() + "  " + ent.getValue() + " "));

                    System.out.println("---------------------------------------");

                    if (length > 4) {
                        double price = 0.0;
                        final Date date = new SimpleDateFormat("yyyy-dd-MM").parse(args[4]);

                        final List<Sale> saleListOnDate = saleList.stream()
                                .filter(sale -> sale.getSale_date().equals(date)).collect(Collectors.toList());

                        for (Sale sale : saleListOnDate) {
                            price += getPrice(bookList, sale.getOnwards());
                        }
                        // sale value on a particular  date
                        System.out.println("Sale on " + args[4]);
                        System.out.println(price);
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static double getPrice(final List<Book> bookList, final String saleOnwards) {
        double priceCalculated = 0.0;
        final String[] splitBookIdWithCount = saleOnwards.split("&");
        for (int i = 0; i < splitBookIdWithCount.length; i++) {
            final String[] idWithCount = splitBookIdWithCount[i].split(";");
            final Book book1 = bookList.stream().filter(bk -> bk.getBook_id().equals(idWithCount[0])).findFirst().orElse(null);
            if (book1 != null)
                priceCalculated += book1.getBook_price() * Integer.parseInt(idWithCount[1]);
        }
        return priceCalculated;
    }
}
