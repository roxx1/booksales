package com.company.models;

public enum SalePaymentEnum {
    CASH("CASH"), CARD("CARD");

    private final String paymentType;

    SalePaymentEnum(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getPaymentType() {
        return paymentType;
    }
}
