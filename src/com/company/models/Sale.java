package com.company.models;

import java.util.Date;

public class Sale {

    private Date sale_date;
    private String sale_email;
    private SalePaymentEnum sale_payment_method;
    private int sale_item_count;
    private String onwards;

    public Sale() {
    }

    public Date getSale_date() {
        return sale_date;
    }

    public void setSale_date(Date sale_date) {
        this.sale_date = sale_date;
    }

    public String getSale_email() {
        return sale_email;
    }

    public void setSale_email(String sale_email) {
        this.sale_email = sale_email;
    }

    public SalePaymentEnum getSale_payment_method() {
        return sale_payment_method;
    }

    public void setSale_payment_method(SalePaymentEnum sale_payment_method) {
        this.sale_payment_method = sale_payment_method;
    }

    public int getSale_item_count() {
        return sale_item_count;
    }

    public void setSale_item_count(int sale_item_count) {
        this.sale_item_count = sale_item_count;
    }

    public String getOnwards() {
        return onwards;
    }

    public void setOnwards(String onwards) {
        this.onwards = onwards;
    }

    @Override
    public String toString() {
        return "Sale{" +
            "sale_date=" + sale_date +
            ", sale_email='" + sale_email + '\'' +
            ", sale_payment_method=" + sale_payment_method +
            ", sale_item_count=" + sale_item_count +
            ", onwards='" + onwards + '\'' +
            '}';
    }
}
